#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "SimpleLL.h"
int printMenu();

int main()
{

    int breakCheck = 1;
    int userSelection;

    // run until case 5
    while (breakCheck == 1)
    {

        // print menu and based on user input run proper function
        printf("\n");
        int selection = printMenu();
        switch (selection)
        {
        case 1:
            printList();
            break;
        case 2:
            // ask for and scan for user input
            printf("what number would you like to append? ");
            scanf("%d", &userSelection);
            append(userSelection);
            printList();
            break;
        case 3:
            // ask for and scan for user input
            printf("what number would you like to append to the front? ");
            scanf("%d", &userSelection);
            addFront(userSelection);
            printList();
            break;
        case 4:
            deleteList();
            printList();
            break;
        case 5:
            deleteList();
            breakCheck = 0;
            break;
        }
    }

    return 0;
}
